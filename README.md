# Example for diploma

This is example project of how can CPack be used to package software.

## Build

```bash
mkdir build
cd build
cmake ../
make
make package
```
