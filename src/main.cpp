#include <diploma_example/some_header.h>

#include <iostream>

int main()
{
    std::cout << DiplomaExample::get_system_info() << '\n';

    return 0;
}
